//
//  Assigment1.swift
//  LES54
//
//  Created by Vladyslav Vdovychenko on 25.04.17.
//  Copyright © 2017 Vladyslav Vdovychenko. All rights reserved.
//

import UIKit

class Assigment1: NSObject {
    static func nameLenght(name:NSString) -> Int {
        let ourName = name
        var nameLength = name.length
        print("Length of my name = \(nameLength)")
        return nameLength
    } //задача 1
    static func patronymic(patr:NSString) {
        if patr.hasSuffix("ич") {
            print("Вы правильно ввели мужское отчество")
        }
        else {
            print("Вы ошиблись")
        }
    } // задача 2
    static func password(pass:String) -> Int {
        var value = 0
        for char in pass.characters {
            if ("0"..."9").contains(char) {
                value+=1
                break
            }
        } //цифры
        for char in pass.characters {
            if ("a"..."z").contains(char) {
                value+=1
                break
            }
        } //нижний регистр
        for char in pass.characters {
            if ("A" ... "Z").contains(char) {
                value+=1
                break
            }
            
        } //верхний регистр
        for char in pass.characters {
            if ("-"..."_").contains(char) {
                value+=1
                break
            }
            
        } //символы
        if value == 4 {
            value = 5
        }
        
        
        print("Надежность пароля: \(value)")
        return value
    } //задача 6
    static func sotr(sortWord:String) {
        var names = ["lada", "sedan", "baklazhan"]
        var newNames = [String]()
        for i in 0..<names.count {
            if names[i].contains("da") {
                newNames.append(names[i])
            }
        }
        print(newNames)
    } //задача 9
    
}

