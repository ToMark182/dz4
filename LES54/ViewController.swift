//
//  ViewController.swift
//  LES54
//
//  Created by Vladyslav Vdovychenko on 25.04.17.
//  Copyright © 2017 Vladyslav Vdovychenko. All rights reserved.
//

import UIKit
//при каждом нажатии на экран будет выводится результат выполнения каждой задачи

class ViewController: UIViewController {
    var xPosition: CGFloat = 100
    var yPosition: CGFloat = 260
    let boxes: CGFloat = 35
    var touch = 0
    var levels = 3
    var newWidth:CGFloat = 200
    var newHeight:CGFloat = 200
    var ourColor:UIColor = .black
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .yellow
    }
    
    
    func drawBox(xPos:CGFloat,yPos:CGFloat) {
        let box = UIView()
        box.frame.size.width = 30
        box.frame.size.height = 30
        box.frame.origin.x = xPos
        box.frame.origin.y = yPos
        box.backgroundColor = UIColor.black
        view.addSubview(box)
        
    } //коробка
    func newBox(width: CGFloat,height: CGFloat,color:UIColor,xPos:CGFloat,yPos:CGFloat) {
        let box = UIView()
        box.frame.size.width = width
        box.frame.size.height = height
        box.frame.origin.x = xPos
        box.frame.origin.y = yPos
        box.backgroundColor = color
        view.addSubview(box)
        
    } //коробка для 4 задания
    
    override func touchesBegan(_ touches: Set<UITouch>, with: UIEvent?) {
        touch+=1
        if touch % 5 == 1 {
            for _ in 0..<3 {
                drawBox(xPos: xPosition, yPos: yPosition)
                xPosition+=boxes
            }
        }
        if touch % 5 == 2 {
            for obj in view.subviews {
                obj.removeFromSuperview()
            }
            xPosition = 100
            view.backgroundColor = .red
            for _ in 0..<3{
                for _ in 0..<levels {
                    drawBox(xPos: xPosition, yPos: yPosition)
                    xPosition+=boxes
                    
                }
                xPosition = 100
                yPosition-=35
                levels-=1
            }
        }
        if touch % 5 == 3 {
            for obj in view.subviews {
                obj.removeFromSuperview()
            }
            levels = 3
            xPosition = 100
            yPosition = 260
            view.backgroundColor = .blue
            for i in 0..<3 {
                if i == 2 {
                    xPosition=135
                }
                for _ in 0..<levels {
                    drawBox(xPos: xPosition, yPos: yPosition)
                    xPosition+=boxes
                    
                }
                xPosition -= 85
                yPosition-=35
                levels-=1
            }
            
        }
        if touch % 5 == 4 {
            view.backgroundColor = UIColor.green
            for obj in view.subviews {
                obj.removeFromSuperview()
            }
            for i in 0..<4 {
                if i == 1 {
                    ourColor = .darkGray
                }
                else if i == 2 {
                    ourColor = .blue
                }
                else if i == 3 {
                    ourColor = .red
                }
                newWidth -= 40
                newHeight -= 40
                xPosition += 20
                yPosition += 20
                newBox(width: newWidth, height: newHeight, color: ourColor,xPos:xPosition,yPos: yPosition)
               
            }
            
            
        }
        if touch % 5 == 0 {
            for obj in view.subviews {
                obj.removeFromSuperview()
            }
            xPosition = 100
            yPosition = 260
            newHeight = 200
            newWidth = 200
            levels = 3
            ourColor = .black
        }
    }
}


